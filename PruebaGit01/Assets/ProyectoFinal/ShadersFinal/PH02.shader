﻿Shader "Custom/PH02" {
	Properties {
		_MainTint ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SpecularColor("Specular color", Color) = (1,1,1,1)
		_SpecPower ("Specular power", Range(1,30)) = 1
		_NormalMap("Normal Map", 2D) = "bump" {}
		_NMI("Normal Map Intensity", Range(0, 10)) = 5.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Phong

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		float4 _SpecularColor;
		float4 _MainTint;
		float _SpecPower;
		sampler2D _NormalMap;
		fixed _NMI;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};

		fixed4 LightingPhong(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten){
			float NdotL = dot(s.Normal, lightDir);
			float3 reflectionVector = normalize(2.0 * s.Normal * NdotL - lightDir);

			float spec = pow(max(0, dot(reflectionVector, viewDir)), _SpecPower);
			float3 finalSpec = _SpecularColor.rgb * spec;

			fixed4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * max(0, NdotL) * atten) + (_LightColor0.rgb * finalSpec);
			c.a = s.Alpha;
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _MainTint;
			float3 normalMap = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
			normalMap.x *= _NMI;
			normalMap.y *= _NMI;
			o.Normal = normalize(normalMap);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
