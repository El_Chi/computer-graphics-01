﻿Shader "Custom/NM01" {
	Properties {
		_Color(" Color", Color) = (0,0,0,0)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap("Normal Map", 2D) = "bump" {}
		_NMI("Normal Map Intensity", Range(0, 5)) = 5.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		fixed _NMI;
		fixed4 _Color;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};

		UNITY_INSTANCING_CBUFFER_START(Props)
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			float3 normalMap = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
			normalMap.x *= _NMI;
			normalMap.y *= _NMI;
			o.Normal = normalize(normalMap);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
