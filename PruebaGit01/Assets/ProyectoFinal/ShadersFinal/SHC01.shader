﻿Shader "Custom/SHC01" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DotProduct("Rim effect", Range(-1, 1)) = 0.25
		_AmbientColor("Ambient Color", Color) = (1, 1, 1, 1)
		_ChangeShield("Changing Shield", Range(0, 2)) =	1
	}
	SubShader {
		Tags { "RenderType"="Opaque" 
		"Queue"="Transparent"
		"IgnoreProjector"="True"
		}
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert alpha:fade

		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 worldNormal;
			float3 viewDir;
		};

		fixed4 _Color;
		float _DotProduct;
		fixed4 _AmbientColor;
		float _ChangeShield;

		void surf (Input IN, inout SurfaceOutput o) {
			float4 c = tex2D(_MainTex, IN.uv_MainTex) * clamp(_ChangeShield, _Color, _AmbientColor);
			o.Albedo = c.rgb;

			float border = 1 - (abs(dot(IN.viewDir, IN.worldNormal)));
			float alpha = (border * (1 - _DotProduct) + _DotProduct);
			o.Alpha = c.a * alpha;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
