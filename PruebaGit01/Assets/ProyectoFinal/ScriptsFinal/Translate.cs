﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translate : MonoBehaviour {

    [SerializeField]
    private Transform originalPos;
    [SerializeField]
    private Transform targetPos;

    void Start () {
        transform.position = originalPos.position;
    }

    void Update () {
        transform.position = new Vector3(transform.position.x - Time.deltaTime * 4, targetPos.position.y + Time.deltaTime, targetPos.position.z + Time.deltaTime);
    }
}
