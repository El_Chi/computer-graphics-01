﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggered : MonoBehaviour {

    [SerializeField]
    private Transform targetPos;

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(targetPos.position.x + Time.deltaTime * 2, targetPos.position.y + Time.deltaTime * 2, targetPos.position.z + Time.deltaTime * 2);
    }
}
