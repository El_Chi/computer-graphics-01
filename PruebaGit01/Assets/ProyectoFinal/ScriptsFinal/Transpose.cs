﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transpose : MonoBehaviour {

    // public Vector3 TargetPos;

    [SerializeField]
    private Transform originalPos;
    [SerializeField]
    private Transform targetPos;
    bool StartTimer = true;

    void Start () {
        transform.position = originalPos.position;
	}
	
	void Update () {
        // transform.Translate(Vector3.forward * Time.deltaTime);

        // Vector3.Lerp();

        // transform.position = new Vector3(100, 0, 200);

        // transform.position = Vector3.Lerp(transform.position, TargetPos, Time.deltaTime);

        // transform.position = Vector3.Lerp(transform.position, targetPos.position, Time.deltaTime);
        // transform.rotation = Quaternion.Lerp(transform.rotation, targetPos.rotation, Time.deltaTime);
        StartCoroutine(CustomUpdate());
    }

    IEnumerator CustomUpdate()
    {
        // print("antes");
        // yield return new WaitForSeconds(2);
        // print("despuès");

        /*float originalLX = transform.position.z;

        while(transform.position.z <= originalLX + 500)
        {
            transform.position = new Vector3(targetPos.position.x, targetPos.position.y, transform.position.z + Time.deltaTime * 20);
            yield return new WaitForEndOfFrame();
        }
         */

        while (StartTimer)
        {
            transform.position = new Vector3(targetPos.position.x, targetPos.position.y, transform.position.z + Time.deltaTime * 25);
            yield return new WaitForSeconds(11);
        }
    }
}
