﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangingShields : MonoBehaviour {

    Renderer renderer;

    void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    void Update()
    {
        float ambient = Mathf.PingPong(Time.time, 3);
        renderer.material.SetFloat("_ChangeShield", ambient);
    }
}
