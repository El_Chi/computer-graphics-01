﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour {

    public Material[] materials = new Material[4];
    Renderer r;
    public Light l;
    bool StartTimer = true;
    public Text inf;
    public Animator camera;

	void Start () {
        r = GetComponent<Renderer>();
        camera.enabled = false;
        l.enabled = false;
    }
    
    private void Update()
    {
        StartCoroutine(changematerials());
        StartTimer = false;
        SetInformation();
    }
    
    IEnumerator changematerials()
    {
       while (StartTimer)
        {
            yield return new WaitForSeconds(2);
            r.material = materials[0];
            yield return new WaitForSeconds(1);
            l.enabled = true;
            yield return new WaitForSeconds(6.5f);
            r.material = materials[1];
            yield return new WaitForSeconds(10f);
            r.material = materials[2];
            yield return new WaitForSeconds(10);
            r.material = materials[3];
            yield return new WaitForSeconds(5);
            camera.enabled = true;
        }
    }
    
    void SetInformation()
    {
        if (r.material.mainTexture == false)
        {
            inf.text = "Shader: " + r.material.shader.name + ".  Texture: " + "None" +
             ".   Color: " + r.material.color.ToString() + ".   Light: " + l.enabled + ".   Timer: " + Time.time.ToString();
        }
        else
        {
            inf.text = "Shader: " + r.material.shader.name + ".  Texture: " + r.material.mainTexture +
             ".   Color: " + r.material.color.ToString() + ".   Light: " + l.enabled + ".   Timer: " + Time.time.ToString();
        }
    }
}
