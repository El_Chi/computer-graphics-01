﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalMap : MonoBehaviour {

    Renderer r;
    void Start () {
        r = GetComponent<Renderer>();
	}
	
	void Update () {
        float nm = Mathf.PingPong(Time.time, 5);
        r.material.SetFloat("_NMI", nm);
        r.material.SetFloat("_AmbClrSld", nm);
    }
}
