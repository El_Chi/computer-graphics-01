﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dummy : MonoBehaviour {

    [Range(0, 10)]

    [HideInInspector]int sampleSliderInteger = 1;
    //[SerializeField]int sampleSliderInteger = 1;


    /*
    struct MyStruct
    {
        public int sampleInt;
        private string sampleString;
    }

    private void Start()
    {
        MyStruct myStruct;
        myStruct.sampleInt = 10;
        print(myStruct.sampleInt);
    }
    */
}
