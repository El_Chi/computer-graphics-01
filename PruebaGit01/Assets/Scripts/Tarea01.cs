﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tarea01 : MonoBehaviour {

    Renderer renderer;

	void Start () {
        renderer = GetComponent<Renderer>();	
            }
	
	void Update () {
        float ambient = Mathf.PingPong(Time.time, 5);
        renderer.material.SetFloat("_AS", ambient);
	}
}
