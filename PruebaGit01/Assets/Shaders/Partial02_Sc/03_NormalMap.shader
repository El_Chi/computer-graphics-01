﻿Shader "Custom/03_NormalMap" {
	Properties {
		_Color("Useless Color", Color) = (0,0,0,0)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap("Normal Map", 2D) = "bump" {}
		_NMI("Normal Map Intensity", Range(0, 5)) = 5.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		fixed _NMI;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};

		UNITY_INSTANCING_CBUFFER_START(Props)
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutput o) {
			fixed2 scrolledUV = IN.uv_NormalMap;
			fixed xScroll = 2 * _Time;
			scrolledUV += fixed2(xScroll, 0);

			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			float3 normalMap = UnpackNormal(tex2D(_NormalMap, scrolledUV));
			normalMap.x *= _NMI;
			normalMap.y *= _NMI;
			o.Normal = normalize(normalMap);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
