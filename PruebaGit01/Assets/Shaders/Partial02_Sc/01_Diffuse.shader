﻿Shader "Custom/01_Diffuse" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_AmbientColor("Ambient Color", Color) = (0, 0, 0, 0)
		_AmbClrSld("Ambient Color Slider", Range(0, 5)) = 2.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};
		fixed4 _Color;
		fixed4 _AmbientColor;
		fixed _AmbClrSld;

		UNITY_INSTANCING_CBUFFER_START(Props)
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = pow((_Color + _AmbientColor), _AmbClrSld);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
