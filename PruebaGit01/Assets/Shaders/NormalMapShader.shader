﻿Shader "Custom/NormalMapShader" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "bump" {}
		_NormalMapIntensity("Normal Map Intensity", Range(0, 1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		float _NormalMapIntensity;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};

		void surf (Input IN, inout SurfaceOutput o) {
		o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		float3 normalMap = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
		normalMap.x *= _NormalMapIntensity;
		normalMap.y *= _NormalMapIntensity;
		o.Normal = normalize(normalMap);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
